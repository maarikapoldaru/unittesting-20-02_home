const validate_is_number_inputs, from = require('./validate_is_number_inputs');

function minus(a, b) {
    validate_is_number_inputs(a, b)
    return a - b;
}

module.exports = minus;
